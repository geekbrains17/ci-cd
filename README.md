# Изучение ci/cd

Использован подход GitLabFlow: ветка master для разработки 
и ветка production для релизов

### Запуск сервера
В папке test-app нужно запустить команду

```
uvicorn server:app --reload
```

### Запуск тестов
В корне проекта нужно запустить 
```
python3 test.py
```

